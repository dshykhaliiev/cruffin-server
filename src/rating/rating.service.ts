import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Rating, RatingDocument } from './schemas/rating.schema';
import { Model } from 'mongoose';
import { CreateRatingDto } from './dto/create-dto.dto';

@Injectable()
export class RatingService {
  constructor(
    @InjectModel(Rating.name) private readonly model: Model<RatingDocument>,
  ) {}

  async findAll(): Promise<Rating[]> {
    return await this.model.find().exec();
  }

  async findOne(id: string): Promise<Rating> {
    return await this.model.findById(id).exec();
  }
  async create(createRating: CreateRatingDto): Promise<Rating> {
    return await new this.model(createRating).save();
  }

  async delete(id: string): Promise<Rating> {
    return await this.model.findByIdAndDelete(id).exec();
  }
}
