import {Body, Controller, Delete, Get, Param, Post} from '@nestjs/common';
import { RatingService } from './rating.service';
import { CreateRatingDto } from './dto/create-dto.dto';

@Controller('ratings')
export class RatingController {
  constructor(private readonly ratingService: RatingService) {}

  @Get()
  async index() {
    return await this.ratingService.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: string) {
    return await this.ratingService.findOne(id);
  }

  @Post()
  async create(@Body() createRatingDto: CreateRatingDto) {
    return await this.ratingService.create(createRatingDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: string) {
    return await this.ratingService.delete(id);
  }
}
