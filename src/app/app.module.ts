import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RatingModule } from '../rating/rating.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://127.0.0.1:27017/rating'),
    RatingModule,
  ],
})
export class AppModule {}
